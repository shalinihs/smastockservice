package com.vitasoft.tech.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.entity.Stock;
import com.vitasoft.tech.exception.StockNotFoundException;
import com.vitasoft.tech.repo.StockRepository;
import com.vitasoft.tech.service.IStockService;

@Service
public class StockServiceImpl implements IStockService {

	@Autowired
	private StockRepository repo;

	@Override
	public Long addStock(Stock stock) {

		return repo.save(stock).getId();
	}

	@Override
	public Stock getOneStock(Long id) {
		Optional<Stock> opt = repo.findById(id);
		if (!opt.isPresent()) {
			throw new StockNotFoundException("Given Stock id:" + id + " does not exists");
		} else {

			return opt.get();

		}

	}

	@Override
	public List<Stock> getAllStock() {

		return repo.findAll();
	}

	@Override
	public void updateStock(Stock stock) {
		if (repo.existsById(stock.getId())) {
			repo.save(stock);
		}

	}

}
