package com.vitasoft.tech.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.Stock;

public interface StockRepository extends JpaRepository<Stock,Long> {

}
