package com.vitasoft.tech.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.vitasoft.tech.exception.StockNotFoundException;
import com.vitasoft.tech.payload.response.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionHandler {
	

	/*
	 * If  CompanyNotFoundException is thrown from any Restcontroller
	 * then below method is executed and returns error message with 500 status code
	 * It is like reusable catch block code
	 * 
	 * */
	
	@ExceptionHandler(StockNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleStockNotFoundExeption(StockNotFoundException snfe){
		
		return ResponseEntity.internalServerError().body(new ErrorMessage(new Date().toString(),
				snfe.getMessage(),
				HttpStatus.INTERNAL_SERVER_ERROR.value(),
				HttpStatus.INTERNAL_SERVER_ERROR.name()
				));
		
	}
	
	
	
	

}
