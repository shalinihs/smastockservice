package com.vitasoft.tech.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.entity.Stock;
import com.vitasoft.tech.service.IStockService;

@RestController
@RequestMapping("/stock")
public class StockRestController {

	@Autowired
	IStockService service;

//	1.Add Stocks
	@PostMapping("/create")
	public ResponseEntity<String> createStock(@RequestBody Stock stock) {

		Long id = service.addStock(stock);
		return ResponseEntity.ok("Stock Registered'" + id + "'");
	}

//	2.Fetch all Stock
	@GetMapping("/all")
	public ResponseEntity<List<Stock>> fetchAllStock() {
		List<Stock> list = service.getAllStock();
		return ResponseEntity.ok(list);

	}

//	3.Fetch One Stock by id
	@GetMapping("/find/{id}")
	public ResponseEntity<Stock> fetchOneStock(@PathVariable Long id) {

		Stock stock = service.getOneStock(id);
		return ResponseEntity.ok(stock);

	}
	
//	4.Update the record
	@PutMapping("/update")
	public ResponseEntity<String> updateStock(@RequestBody Stock stock){
		service.updateStock(stock);
		return ResponseEntity.ok("Stock Updated");
		
	}

}
