package com.vitasoft.tech.service;

import java.util.List;

import com.vitasoft.tech.entity.Stock;

public interface IStockService {
	
	Long addStock(Stock stock);
	Stock getOneStock(Long id);
	List<Stock> getAllStock();
	void updateStock(Stock stock);
	

}
