package com.vitasoft.tech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="stock_tab")
public class Stock {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="stock_id_col")
	
	private Long id;
	
	@Column(name="stock_code_col")
	private String code;
	
	@Column(name="stock_desc_col")
	private String description;
	
	@Column(name="stock_cost_col")
	private  Double costPerUnit;
	
	@Column(name="stock_lot_size_col")
	private Integer batchLotSize;
	
	@Column(name="stock_lot_max_col")
	private Integer maxLotsCust;
	
	@Column(name="stock_type_col")
	private String stocktype; //BSE/NSE
	
	@Column(name="stock_consumer_id_col")
	private String regConsumerId;
	
	@Column(name="stock_source_mode_col")
	private String sourceMode;
	
	
//	TODO:private Company company;
	
	

}
